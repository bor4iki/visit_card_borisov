import asyncio

from aiogram import Dispatcher
from aiogram.dispatcher import FSMContext
from aiogram.types import Message, ReplyKeyboardRemove, CallbackQuery, ContentType

from tgbot.keyboards.inline import first_inline_buttons, second_inline_buttons
from tgbot.keyboards.reply import resize_keyboard, YES_keyboard, first_buttons, continue_button, phone_number, \
    location_keyboard, start_button
from tgbot.misc.states import GetContact

from tgbot.services.api_cbrf import get_curs
from tgbot.services.api_geocoding import get_address_from_coords
from tgbot.services.get_logger import logger


async def user_start(message: Message):
    full_name = message.from_user.full_name
    logger().info(f'{full_name}/{message.from_user.id} start use bot')
    await message.answer("Итак, начнем знакомство!\n\n"
                         "Давай я угадаю твое имя ....",
                         reply_markup=ReplyKeyboardRemove())
    await asyncio.sleep(2)
    await message.answer("Подожди я еще думаю....")
    await asyncio.sleep(2)
    await message.answer(f"Звезды подсказали мне что тебя зовут: <b>{full_name}</b>\n"
                         f"Хочешь узнать как я это сделал?\n"
                         f"Нажимай на кнопки ниже, кстати, это тоже фишки телеграмм ботов, которые доступны мне",
                         reply_markup=first_buttons)


async def user_say_YES(message: Message):
    hide_buttons = ReplyKeyboardRemove()
    logger().info(f'{message.from_user.full_name}/{message.from_user.id} say YES')
    await message.answer(f"Сейчас я тебе покажу, какой строчкой в программе я это сделал. Только не пугайся. "
                         f"Если честно, я и сам не понимаю 😉\n\n"
                         f"<code>full_name = message.from_user.full_name</code>\n\n"
                         f"Как ты уже заметил, я умею играться со шрифтами, так что если нужно сделать "
                         f"<i>красивый текст</i> - я это <b>могу</b>!", reply_markup=hide_buttons)
    await asyncio.sleep(5)
    await message.answer(f"Надеюсь тебе понравилось, но дальше больше!!\n"
                         f"Ты уже увидел большие кнопки, теперь смотри какие они еще бывают.",
                         reply_markup=resize_keyboard)


async def user_say_NO(message: Message):
    logger().info(f'{message.from_user.full_name}/{message.from_user.id} say NO')
    await message.answer(f"Ну и как это называется??? Я для тебя старался, а ты не смотришь...\n\n"
                         f"Давай попробуем еще раз\n\n"
                         f"Хочешь узнать, как я угадал твоё имя?", reply_markup=YES_keyboard)


async def user_say_continue(message: Message):
    logger().info(f'{message.from_user.full_name}/{message.from_user.id} looks at inline buttons')
    await message.answer("Отлично ты посмотрел на аккуратные кнопки и то как они могут быть расположены.",
                         reply_markup=ReplyKeyboardRemove())
    await asyncio.sleep(2)
    await message.answer("Теперь хочу тебе показать второй вид кнопок. Они называются Inline-кнопки\n\n"
                         "Выглядят они примерно так 👇👇", reply_markup=first_inline_buttons)


async def first_inline(call: CallbackQuery):
    if str(call.data) == "inline_1":
        logger().info(f'{call.from_user.full_name}/{call.from_user.id} call 1 inline')
        await call.answer(text='Смотри какая может быть реакция')
    elif str(call.data) == 'inline_2':
        logger().info(f'{call.from_user.full_name}/{call.from_user.id} call 2 inline')
        await call.answer(text='И даже такая', show_alert=True)
    elif str(call.data) == 'inline_3':
        # print(call)
        logger().debug(f'{call.from_user.full_name}/{call.from_user.id} call 3 inline')
        # print('here')
    elif str(call.data) == 'inline_4':
        logger().info(f'{call.from_user.full_name}/{call.from_user.id} call 4 inline')
        await call.message.edit_text('Ой, кажется ты что то сломал...\n\n'
                                     'Но не переживай!!\n\n'
                                     'Продолжаем?')
        await call.message.edit_reply_markup(reply_markup=second_inline_buttons)
    else:
        logger().info(f'{call.from_user.full_name}/{call.from_user.id} call 5 inline')
        await call.answer()
        await call.message.answer("Двигаемся дальше, есть у меня еще пару фишек для тебя!")
        await asyncio.sleep(2)
        await call.message.answer_photo('https://i.ytimg.com/vi/XV74YXTbw5g/maxresdefault.jpg',
                                        caption='Как видишь, я умею добавлять описание к картинке, это не так сложно!',
                                        reply_markup=continue_button)


async def continue_button_next(message: Message):
    usd, eur = get_curs()
    text = f'<b>Курс валют на текущий момент по курсу центробанка</b>:\n\n' \
           f'Доллар : <i>{round(usd, 2)}</i>\n' \
           f'Евро : <i>{round(eur, 2)}</i>\n\n' \
           f'Проверить можно <a href="https://cbr.ru/currency_base/daily/">тут</a>'
    logger().info(f'{message.from_user.full_name}/{message.from_user.id} look at curs and start State')
    await message.answer(f'{text}', reply_markup=ReplyKeyboardRemove())
    await asyncio.sleep(4)
    await GetContact.phone_number.set()
    await message.answer('Ну вот, я показал тебе практически все фишки.\n\n'
                         'Осталось совсем чуть-чуть.\n\n'
                         'Давай представим, что тебе нужны номера телефонов твоих клиентов,'
                         ' которые будут пользоваться ботом. Я могу помочь с этим.', reply_markup=phone_number)


async def get_phone_number(message: Message, state: FSMContext):
    logger().info(f'{message.from_user.full_name}/{message.from_user.id} get phone')
    phone = message.contact.phone_number[1:]
    logger().info(f'{message.from_user.full_name}/{message.from_user.id} {phone}')
    total = 0
    for i in phone:
        total += int(i)
    async with state.proxy() as data:
        data['phone_number'] = phone
    await GetContact.location.set()
    await message.answer(f"Вау супер, ты дал мне свои контакты!!\n\n"
                         f"А ты знал, что сумма чисел твоего номера равна <b>{total}</b>",
                         reply_markup=ReplyKeyboardRemove())
    await asyncio.sleep(2)
    await get_location(message)


async def not_phone_number(message: Message, state: FSMContext):
    logger().info(f'{message.from_user.full_name}/{message.from_user.id} do not get phone')
    async with state.proxy() as data:
        data['phone_number'] = None
    await GetContact.location.set()
    await message.answer('Ой ну и правильно, что не дал свой номер телефона.\n'
                         'Те ребята, которые были посмелее, уже выбрасывают симку... Им начали звонить из банков.',
                         reply_markup=ReplyKeyboardRemove())
    await asyncio.sleep(1)
    await message.answer_sticker(r'CAACAgIAAxkBAAICM2LP4cQIMuZJMSz6cpMmEXLHoRYXAAKmAAP3AsgPqwzk86kqxlgpBA')
    await asyncio.sleep(2)
    await get_location(message)


async def get_location(message: Message):
    logger().info(f'{message.from_user.full_name}/{message.from_user.id} ask location')
    await message.answer("Окей, я еще могу запросить твои координаты 😏\n\n"
                         "<code>(Данная функция работает только в мобильной версии приложения)</code>\n\n"
                         "Дай мне их!", reply_markup=location_keyboard)


async def get_all_info(message: Message, state: FSMContext):
    logger().info(f'{message.from_user.full_name}/{message.from_user.id} get location')
    location = message.location
    async with state.proxy() as data:
        data['location'] = location

    latitude = data['location']['latitude']
    longitude = data['location']['longitude']
    coords = get_address_from_coords(latitude, longitude)
    logger().info(f'{message.from_user.full_name}/{message.from_user.id} {coords}')
    await state.finish()
    await message.answer_animation('https://i.gifer.com/758R.gif', caption='О даааа, я сейчас скажу, где ты находишься',
                                   reply_markup=ReplyKeyboardRemove())
    await asyncio.sleep(5)
    await message.answer(text=f'Фуххх, смог понять где ты сейчас находишься\n\n'
                              f'<code>{coords}</code>\n\n'
                              f'Не переживай, эта информация останется только между нами и...')
    await asyncio.sleep(2)
    await last_message(message)


async def not_get_location(message: Message, state: FSMContext):
    logger().info(f'{message.from_user.full_name}/{message.from_user.id} do not get location')
    await state.finish()
    await message.answer("Очень жаль, что ты не дал мне свои координаты (\n"
                         "Там была прикольная анимация )\n"
                         "Но ничего, чуть позже я дам тебе возможность, пройти это все заново,"
                         " надеюсь ты выберешь правильный путь 😉", reply_markup=ReplyKeyboardRemove())
    await asyncio.sleep(2)
    await last_message(message)


async def last_message(message: Message):
    logger().info(f'{message.from_user.full_name}/{message.from_user.id} last message')
    await message.answer("Ну вот и закончилось наше с тобой знакомство.\n"
                         "Было очень приятно с тобой пообщаться!! Ты просто космос!\n"
                         "Мы с тобой пробежались по всем основным фишкам телеграмма, попробовали потыкать кнопки,"
                         "ты посмотрел, как можно оформить текст. Но ты не поверишь, это все ограничевается только "
                         "фантазией!\n\n"
                         "Если тебе понравилось и ты имеешь безграничную фантазию, "
                         "я оставлю тебе контакты Бориса, можешь ему написать и вы вместе сможете сотворить "
                         "<b>НЕВОЗМОЖНОЕ</b>.\n"
                         "Предлагаю тебе перейти по кнопке контакты, чтоб получше познакомиться с Борисом.\n"
                         "А если у тебя есть желание пройти еще раз этот увлекательный интерактив - жми старт."
                         "<i>Ниже кнопка, чтоб пройти заново</i>", reply_markup=start_button)


def register_user(dp: Dispatcher):
    dp.register_message_handler(user_start, commands=["start"], state="*")
    dp.register_message_handler(user_say_YES, text=['ДА'])
    dp.register_message_handler(user_say_NO, text=['НЕТь'])
    dp.register_message_handler(user_say_continue, text=['Продолжить'])
    dp.register_message_handler(continue_button_next, text=['Дальше'])
    dp.register_message_handler(not_get_location, text=['Я на планете Земля'], state=GetContact.location)
    dp.register_message_handler(not_phone_number, text=['Нет не дам!!'],
                                state=GetContact.phone_number)
    dp.register_callback_query_handler(first_inline, text_startswith=['inline_'])
    dp.register_message_handler(get_phone_number, state=GetContact.phone_number, content_types=ContentType.CONTACT)
    dp.register_message_handler(get_all_info, state=GetContact.location, content_types=ContentType.LOCATION)
