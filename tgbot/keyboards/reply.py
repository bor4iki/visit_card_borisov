from aiogram.types.reply_keyboard import ReplyKeyboardMarkup, KeyboardButton

first_buttons = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text='ДА'),
            KeyboardButton(text='НЕТь'),
        ],
    ]
)

resize_keyboard = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text='Продолжить'),
        ],
        [
            KeyboardButton(text='Продолжить'),
            KeyboardButton(text='Продолжить'),
            KeyboardButton(text='Продолжить'),
        ],
        [
            KeyboardButton(text='Продолжить'),
            KeyboardButton(text='Продолжить'),
        ]
    ],
    resize_keyboard=True
)


YES_keyboard = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text='ДА'),
        ],
    ]
)

continue_button = ReplyKeyboardMarkup(
    keyboard=
    [
        [
            KeyboardButton(text='Дальше'),
        ],
    ],
    resize_keyboard=True
)

phone_number = ReplyKeyboardMarkup(
    keyboard=
    [
        [
            KeyboardButton(text='Дам свой номер телефона', request_contact=True),
        ],
        [
            KeyboardButton(text='Нет не дам!!')
        ]
    ],
    resize_keyboard=True
)

location_keyboard = ReplyKeyboardMarkup(
    keyboard=
    [
        [
            KeyboardButton(text="Показать где я сейчас", request_location=True),
        ],
        [
            KeyboardButton(text="Я на планете Земля")
        ]
    ],
    resize_keyboard=True
)

start_button = ReplyKeyboardMarkup(
    keyboard=
    [
        [
            KeyboardButton(text='/start'),
        ],
        [
            KeyboardButton(text='/contacts'),
        ],
    ],
    resize_keyboard=True
)
