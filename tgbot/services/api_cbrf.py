from cbrf.models import DailyCurrenciesRates


def get_curs():
    curs = DailyCurrenciesRates()
    usd = curs.get_by_id("R01235").value
    eur = curs.get_by_id("R01239").value
    return usd, eur
