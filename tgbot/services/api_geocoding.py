import requests

from tgbot.config import load_config


def get_address_from_coords(latitude, longitude):
    config = load_config(".env")

    PARAMS = {
        "apikey": config.ya_api.ya_token,
        "format": "json",
        "lang": "ru_RU",
        "kind": "house",
        "geocode": f'{longitude}, {latitude}'
    }

    # отправляем запрос по адресу геокодера.
    try:
        r = requests.get(url="https://geocode-maps.yandex.ru/1.x/", params=PARAMS)
        # получаем данные
        json_data = r.json()
        # вытаскиваем из всего пришедшего json именно строку с полным адресом.
        address_str = json_data["response"]["GeoObjectCollection"]["featureMember"][0]["GeoObject"]["metaDataProperty"][
            "GeocoderMetaData"]["AddressDetails"]["Country"]["AddressLine"]
        # возвращаем полученный адрес
        return address_str
    except Exception as e:
        # если не смогли, то возвращаем ошибку
        return "error"


if __name__ == '__main__':
    # даем запрос на получение адреса с координатами 37.617585, 55.751903
    address_str = get_address_from_coords(55.771608, 37.68755)
    # распечатываем адрес
    print(address_str)
